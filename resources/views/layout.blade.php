<!DOCTYPE>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link href='{{ asset('css/style.css') }}' rel="stylesheet">
    @stack('styles')

    <link href='{{ asset('css/navbar.css') }}' rel="stylesheet">

</head>
<body>
     <div class="page-container">
        <header>

            @if (Route::has('login'))
                <div class="navbar flex-row">
                    <a id="logo" href="/"><img src="storage/logo.png" alt="logo"/></a>
                        @include('header')
                </div>
            @endif
        </header>


        @yield('content')
     </div>

</body>
</html>

