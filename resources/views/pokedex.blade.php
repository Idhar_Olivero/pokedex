@extends('layout')
@push('styles')
    <link href='{{ asset('css/pokemon_list.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_stat-table.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_type.css') }}' rel="stylesheet">
    <link href='{{ asset('css/catch-bar.css') }}' rel="stylesheet">
@endpush

@section('content')

<div class="flex-column full-height-container">
    <form class="catch-form" method="GET" action="/pokedex">
        {{ csrf_field() }}
        <div id="search-bar" class="catch-bar flex-row">

            <div class="catch-element flex-row">
                <img src="{{ asset('storage/rotating.png') }}" class="glyph-refresh" id="refresh-button">
            </div>

            <div class="catch-element flex-row">
                <label class="catch-label">
                    IdPokemon
                    <input name="id" class="catch-input_text" type="number" value="{{ ($filtri['id'] != "") ? $filtri['id'] : "" }}">
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Name
                    <input name="name" class="catch-input_text" type="text" value="{{ ($filtri['name'] != "") ? $filtri['name'] : "" }}">
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Type 1
                    <select name="type1" class="catch-select">
                        <option name="none" value="none">
                            <div class="pokemon_name">None</div>
                        </option>
                        @foreach($types as $type)
                            <option name="IdPokemon" value="{{ $type -> type1 }}" {{ (isset($filtri['type1']) && $filtri['type1'] == $type -> type1) ? 'selected' : ""}}>
                                <div class="pokemon_name {{ $type -> type1 }}">{{ $type -> type1 }}</div>
                            </option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Type 2
                    <select name="type2" class="catch-select">
                        <option name="none" value="none">
                            <div class="pokemon_name">None</div>
                        </option>
                        @foreach($types as $type)
                            <option name="IdPokemon" value="{{ $type -> type1 }}" {{ (isset($filtri['type2']) && $filtri['type2'] == $type -> type1) ? 'selected' : ""}}>
                                <div class="pokemon_name {{ $type -> type1 }}">{{ $type -> type1 }}</div>
                            </option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Egg group
                    <select name="eggGroups" class="catch-select">
                        <option name="none" value="none">
                            <div class="pokemon_name">None</div>
                        </option>
                        @foreach($eggs as $egg)
                            <option name="IdPokemon" value="{{ $egg -> eggGroups }}" {{ (isset($filtri['eggGroups']) && $filtri['eggGroups'] == $egg -> eggGroups) ? 'selected' : ""}}>
                                <div class="pokemon_name">{{ $egg -> eggGroups }}</div>
                            </option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="catch-element flex-row">
                <button type="submit" class="catch-button noSelect">Catch!</button>
            </div>
        </div>
    </form>


    <div class="centered">
        <div class="pokedex-screen">


            <ul class="pokemon_list">


            @foreach($pokemons as $pokemon)

                <li class="pokemon_list-item flex-row">

                    <div class="flex-column pokemon_name-description">
                        <div class="flex-row">
                            <span class="pokemon_number">{{ 'N°' }}{{ $pokemon -> id }} - </span>
                            <h3 class="pokemon_name {{ $pokemon -> type1 }}">{{ $pokemon -> name }}</h3>
                        </div>
                        <p class="pokemon_description">{{ $pokemon -> description }}</p>
                        <div class="flex-row flex-start align-items type-box">

                            <span class="pokemon_type-item pokemon_type-{{ $pokemon -> type1 }}">{{ $pokemon -> type1 }}</span>
                            <span class="empty-space"></span>
                            <span class="pokemon_type-item pokemon_type-{{ $pokemon -> type2 }}">{{ $pokemon -> type2 }}</span>

                        </div>
                    </div>

                    <div class="pokemon_stat-table-container">
                        <table class="pokemon_stat-table">

                            <thead>
                                <tr class="pokemon_stat-table-row">
                                    <th colspan="2" class="pokemon_stat-table-title">Base Stat</th>
                                </tr>

                            </thead>

                            <tbody>
                                <tr class="pokemon_stat-table-row hp">
                                    <th class="pokemon_stat-table-row-title">baseHp</th>
                                    <td class="pokemon_stat-table-value">{{ $pokemon -> baseHp }}</td>
                                </tr>
                                <tr class="pokemon_stat-table-row atk">
                                    <th class="pokemon_stat-table-row-title">baseAttack</th>
                                    <td class="pokemon_stat-table-value">{{ $pokemon -> baseAttack }}</td>
                                </tr>
                                <tr class="pokemon_stat-table-row def">
                                    <th class="pokemon_stat-table-row-title">baseDefence</th>
                                    <td class="pokemon_stat-table-value">{{ $pokemon -> baseDefense }}</td>
                                </tr>
                                <tr class="pokemon_stat-table-row spd">
                                    <th class="pokemon_stat-table-row-title">baseSpeed</th>
                                    <td class="pokemon_stat-table-value">{{ $pokemon -> baseSpeed }}</td>
                                </tr>
                                <tr class="pokemon_stat-table-row atksp">
                                    <th class="pokemon_stat-table-row-title">baseSpecialAttack</th>
                                    <td class="pokemon_stat-table-value">{{ $pokemon -> baseSpecialAttack }}</td>
                                </tr>
                                <tr class="pokemon_stat-table-row defsp">
                                    <th class="pokemon_stat-table-row-title">baseSpecialDefence</th>
                                    <td class="pokemon_stat-table-value">{{ $pokemon -> baseDefense }}</td>
                                </tr>
                            </tbody>


                        </table>
                    </div>

                    <img alt="{{ $pokemon -> name }}" src="{{ asset('storage/pokemons/'. $pokemon -> id .'.png') }}" class='pokemon_img col-4'>


                </li>

            @endforeach

                {{ $pokemons -> links() }}
            </ul>
        </div>
    </div>
</div>

<script>
    function refreshSearch() {
        $('.catch-input_text').val("");
        $('option[name = "none"]').prop("selected",true);
    }
    $('#refresh-button').on('click', function () {refreshSearch();});
</script>
@endsection
