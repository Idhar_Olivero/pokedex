@extends('layout')

@push('styles')

    <link href='{{ asset('css/style.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_list.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_stat-table.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_type.css') }}' rel="stylesheet">
    <link href='{{ asset('css/catch-bar.css') }}' rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
