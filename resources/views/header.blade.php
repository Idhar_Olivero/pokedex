<?php $allenatore = Auth::user(); ?>

@guest
    <div class="navbar-inner flex-row">
        <div class="navbar-welcome">Hello there, <span class="Water" style="font-size: 30px">trainer</span></div>
        <ul class="nav flex-row">
            <li class="hover-li">
                <div class="hover-div">
                    <a class="menu-link hover-a" href="{{ route('login') }}">
                        Login
                    </a>
                </div>
            </li>

            @if (Route::has('register'))
                <li class="hover-li">
                    <div class="hover-div">
                        <a class="menu-link hover-a" href="{{ route('register') }}">
                            Register
                        </a>
                    </div>
                </li>
            @endif
        </ul>
    </div>
@else
    <div class="navbar-inner flex-row">
        <div class="navbar-welcome">
            <span style="padding-bottom: 5px; font-size: 30px;">Hello there,</span>
            <span style="padding-bottom: 5px;" class="allen-nome {{ $allenatore -> prefer_type }}">{{ $allenatore -> nome }}</span>
            <img src="{{ asset('storage/animated/'. $allenatore -> buddyId .'.gif') }}" alt="Compagno" class="best-buddy" />
        </div>
        <ul class="nav flex-row">
            <li class="hover-li"><div class="hover-div"><a class="menu-link hover-a" href="/">Home</a></div></li>
            <li class="hover-li"><div class="hover-div"><a class="menu-link hover-a" href="/pokedex">Pokedex</a></div></li>
            <li class="hover-li"><div class="hover-div"><a class="menu-link hover-a" href="/team">Team</a></div></li>
            <li class="hover-li">
                <div class="dropdown hover-div">
                    <a class="menu-link hover-a" href="#">Profile</a>
                    <div class="dropdown-content">
                        <ul class="dropdown-list nav_alt">
                            <li class="dropdown-list-item hover-li_alt">
                                <div class="hover-div_alt">
                                    <a class="hover-a_alt">{{ 'TOMA' }}</a>
                                </div>
                            </li>
                            <li class="dropdown-list-item hover-li_alt">
                                <div class="hover-div_alt">
                                    <a class="hover-a_alt">{{ 'TOMA' }}</a>
                                </div>
                            </li>
                            <li class="dropdown-list-item hover-li_alt">
                                <div class="hover-div_alt">
                                    <a class="hover-a_alt">{{ 'TOMA' }}</a>
                                </div>
                            </li>
                            <li class="dropdown-list-item hover-li_alt">
                                <div class="hover-div_alt">
                                    <a class="hover-a_alt" href="{{ 'Logout' }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
@endauth




