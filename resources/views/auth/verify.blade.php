@extends('layout')

@push('styles')

    <link href='{{ asset('css/style.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_list.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_stat-table.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_type.css') }}' rel="stylesheet">
    <link href='{{ asset('css/catch-bar.css') }}' rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
