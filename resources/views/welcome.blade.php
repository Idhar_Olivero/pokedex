@extends('layout')

@push('styles')

    <link href='{{ asset('css/pokemon_type.css') }}' rel="stylesheet">
    <link href='{{ asset('css/home.css') }}' rel="stylesheet">
@endpush

@section('content')
        <div class="flex-center position-ref full-height home-container">
            <div class="flex-column">
                <h1 class="title">
                    Pok&eacutehub
                </h1>

                <div class="flex-row home-links">
                    <div id="pokedex-link-container" class="home-links-container">
                        <a class="pokedex-link" href="{{ url('/pokedex') }}">
                            <div class="figure-container">
                                <img src="{{ asset('storage/pokedex_img.png') }}" class="pokedex-img">
                                <div class="img_caption figure-caption">
                                    <div class="caption-title">Pokedex</div>
                                    <div class="caption-text">Esplora il mondo Pokémon! Da solo o in compagnia degli amici!</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div id="team-link-container" class="home-links-container">
                        <a class="pokedex-link" href="{{ url('/team') }}">
                            <img src="{{ asset('storage/ilteamrocket.jpg') }}" class="pokedex-img">
                            <div class="img_caption figure-caption">
                                <div class="caption-title">Team Builder</div>
                                <div class="caption-text">Catturali tutti!</div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>

@endsection
