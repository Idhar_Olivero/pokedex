@extends('layout')
@push('styles')
    <link href='{{ asset('css/style.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_stat-table.css') }}' rel="stylesheet">
    <link href='{{ asset('css/team_list.css') }}' rel="stylesheet">
    <link href='{{ asset('css/team-member_list.css') }}' rel="stylesheet">
    <link href='{{ asset('css/dotted_menu.css') }}' rel="stylesheet">
    <link href='{{ asset('css/pokemon_type.css') }}' rel="stylesheet">
    <link href='{{ asset('css/catch-bar.css') }}' rel="stylesheet">
    <title>Teams</title>
@endpush


@section('content')


<div class="flex-column full-height-container">
    <!--a href="/team/catch">{{ 'Catch\'em all '}}</a-->
    <form class="catch-form" method="POST" action="/team">
        {{ csrf_field() }}
        <div class="catch-bar flex-row">

            <div class="catch-element flex-row">
                <label class="catch-label">
                    Specie
                    <select name="IdPokemon" class="catch-select">
                        @foreach($pokemons as $pokemon)
                            <option name="IdPokemon" value="{{ $pokemon -> id }}" >
                                <div class="pokemon_name {{ $pokemon -> type1 }}">{{ $pokemon -> name }}</div>
                            </option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Soprannome
                    <input name="Soprannome" class="catch-input_text" type="text" />
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Level
                    <input name="LV" class="catch-input_text" type="number" />
                </label>
            </div>
            <div class="catch-element flex-row">
                <label class="catch-label">
                    Team
                    <input name="IdTeam" class="catch-input_text" type="number" />
                </label>
            </div>
            <div class="catch-element flex-row">
                <button type="submit" class="catch-button">Catch!</button>
            </div>
        </div>
    </form>


    <div class="centered">
        <h1 class="page_title">Team of {{ $allenatore->nome }}</h1>
        <ul class="team_list flex-row">

                @foreach($teams as $team)
                <li class="team_list-element flex-row">
                    <div class="team_list-element-container">
                        <h2 class="team_list-element-title">{{ $team-> teamName }}</h2>
                        <ul class="member_list">
                            @foreach($team -> pokemons as $member)
                                <li class="member_list-item flex-row">
                                    <div style="height: fit-content">
                                        <img alt="{{ $member -> pivot -> Soprannome }}" src="{{ asset(($member -> pivot -> sesso == 'f') ? (file_exists('storage/pokemons/female/' . $member -> pivot -> IdPokemon .'.png')) ? 'storage/pokemons/female/' . $member -> pivot -> IdPokemon .'.png' : 'storage/pokemons/' . $member -> pivot -> IdPokemon .'.png' : 'storage/pokemons/' . $member -> pivot -> IdPokemon .'.png') }}" class='pokemon_img col-4'>
                                    </div>
                                    <div class="member_name-description flex-column">
                                        <h3 class="member_name {{ $member -> pokemonInfo($member -> pivot -> IdPokemon) -> type1 }}">{{ $member -> pivot -> Soprannome }}</h3>
                                        <span class="member_lvl">Lv.{{ $member -> pivot -> LV }}</span>
                                    </div>
                                    <div class="flex-column dotted-menu" id="member-menu">
                                        <div class="menu-dot"></div>
                                        <div class="menu-dot"></div>
                                        <div class="menu-dot"></div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endforeach
        </ul>



    </div>


</div>
@endsection
