<?php

namespace App\Http\Controllers;

use App\Pokemon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PokedexController extends Controller
{
    public function index(Request $request){
        $filters = [];



        $filtri = [
            'id' => "",
            'name' => ""
        ];
       /* && array_push($currentFilters, [$key => $value])
        */
        foreach($request -> request as $key => $value) {
            (isset($value) && $value != 'none' && $key != 'page' && $key != '_token') ? array_push($filters, [$key, $value]) && $filtri[$key] = $value : "";
        }

        $pokemons = Pokemon::where($filters) -> paginate(15);
        $types = Pokemon::select('type1') -> distinct() -> get();
        $eggs = Pokemon::select('eggGroups') -> distinct() -> get();


        return view('pokedex', ['pokemons' => $pokemons -> appends(Input::except('page')), 'types' => $types, 'eggs' => $eggs, 'filtri' => $filtri]);
    }





}
