<?php

namespace App\Http\Controllers;

use App\Allenatore;
use App\Pokemon;
use App\Pokemonallenatore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    public function index () {
        /** @var Allenatore $allenatore */
        $allenatore = Auth::user();
        $teams = $allenatore->teams()->get();
        $pokemons = Pokemon::all()->sortBy('name');

        return view('team', ['teams' => $teams, 'allenatore' => $allenatore, 'pokemons' => $pokemons]);
    }

    public function catchEm (Request $request) {
        $pokemonTeamSel = Pokemonallenatore::where('IdTeam', response('IdTeam'))->count();

        if($pokemonTeamSel >= 6) {
            return redirect('/team');
        } else {
            $pokemon = new Pokemonallenatore;
            $pokemon->IdPokemon = request('IdPokemon');
            $pokemon->IdTeam = request('IdTeam');
            $pokemon->LV = request('LV');
            $pokemon->Soprannome = request('Soprannome');
            $pokemon->sesso = "m";
            $pokemon->DataCattura = new \DateTime();
            $pokemon->save();
            return redirect('/team');
        }
    }



}
