<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pokemon extends Model
{


    public function get_prop_table () {
       return $keys = [
            'baseHp' => $this -> baseHp,
            'baseAttack' => $this -> baseAttack,
            'baseDefence' => $this -> baseDefense,
            'baseSpeed'=> $this -> baseSpeed,
            'baseSpecialAttack' => $this -> baseSpecialAttack,
            'baseSpecialDefense' => $this -> baseSpecialDefense
        ];
    }


    public function pokemonInfo($id)
    {
        return Pokemon::where('id', $id)
            ->first();
    }

}
