<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Auth\Passwords\CanResetPassword;
class Allenatore extends Authenticable
{
    protected $table = 'allenatore';
    use Notifiable;


    protected $fillable = [
        'nome', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function teams(): HasMany
    {
        return $this->hasMany(
            'App\Team',
            'idAllenatore',
            'id'
        );
    }








}
