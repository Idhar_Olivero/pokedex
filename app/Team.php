<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Team extends Model
{

    public function pokemons(): BelongsToMany
    {
        return $this->belongsToMany('App\Pokemon',
            'pokemonallenatore',
            'IdTeam',
            'IdPokemon')
            ->withPivot([
                'LV', 'Soprannome', 'sesso', 'IdPokemon', 'IdTeam'
            ]);
    }


}
