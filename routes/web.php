<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pokedex', 'PokedexController@index');



Route::get('/team', 'TeamController@index')->name('team')->middleware('auth');

Route::post('/team', 'TeamController@catchEm')->name('team.catch')->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/notLogged', 'NotLoggedController@index')->name('notlogged');
